import { AuthGuard } from './shared/auth-guard/auth-guard';
import { CommonComponent } from './common/common.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  {
    path: '',
    component: CommonComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'pessoa', loadChildren: () => import('./pessoa/pessoa.module').then(m => m.PessoaModule)}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
